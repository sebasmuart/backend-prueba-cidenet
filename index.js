/* eslint-disable no-console */
const app = require('./src/server');
const mongoose = require('mongoose');
const { MONGO_URI } = require('./src/config');

mongoose.set('useCreateIndex', true);
mongoose
  .connect(MONGO_URI, {
    useNewUrlParser: true,
    useFindAndModify: false,
    useUnifiedTopology: true,
  })
  .then(() => app.start().then())
  .catch(console.log);
