const EmployeeRepositories = require('../../../respositories/Employee/employee.repository');
const mockingoose = require('mockingoose').default;
let employees = require('../../__mock__/employee.mock');
const db = require('../../../models');

describe('Employee Repository Test', () => {
  beforeEach(() => {
    mockingoose.resetAll();
    jest.clearAllMocks();
  });

  it('should return the employees', async () => {
    mockingoose(db.Employee).toReturn(employees.employees, 'find');
    const _employeeRepository = new EmployeeRepositories(db);
    const expected = await _employeeRepository.getAll();
    expect(expected.length).toBeGreaterThan(0);
  });
});
