const moment = require('moment');

function mapperToClientSide(entity, includeFormat = true) {
  const mappedData = entity.map((data) => ({
    id: data._id,
    firstName: data.firstName,
    secondName: data.secondName ? data.secondName : null,
    first_lastName: data.first_lastName,
    second_lastName: data.second_lastName,
    state: data.state,
    country: data.country,
    email: data.email ? data.email : null,
    typeId: data.typeId,
    uId: data.uId,
    jobArea: data.jobArea,
    admissionDate: includeFormat
      ? moment(data.admissionDate).format('DD-MM-YYYY')
      : data.admissionDate,
    registerDate: moment(data.createdAt).format('DD-MM-YYYY'),
  }));
  return mappedData;
}

module.exports = { mapperToClientSide };
