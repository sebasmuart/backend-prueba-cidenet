const BaseServices = require('../base.services');
const { mapperToClientSide } = require('../../services/dtos/Employee.dto');

let _employeeRepository = null;
class EmployeeServices extends BaseServices {
  constructor({ employeeReporitory }) {
    _employeeRepository = employeeReporitory;
    super(employeeReporitory);
  }

  async searchByValues(value) {
    const employees = await _employeeRepository.searchByValues(value);
    return mapperToClientSide(employees);
  }
}

module.exports = EmployeeServices;
