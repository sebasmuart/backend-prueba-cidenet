const { mapperToClientSide } = require('../services/dtos/Employee.dto');
const { getEmail } = require('../utils/mixins');

let _modelRepository = null;
class BaseServices {
  constructor(modelRepository) {
    _modelRepository = modelRepository;
  }

  async getAll(pageSize, pageNum) {
    const employees = await _modelRepository.getAll(pageSize, pageNum);
    const totalEmployees = await _modelRepository.getCount();
    return { employees: mapperToClientSide(employees), totalEmployees };
  }

  async get(id) {
    const employee = await _modelRepository.get(id);
    return mapperToClientSide([employee], false);
  }

  async create(entity) {
    let email = entity.email;
    const valueRegex = new RegExp(
      `${entity.firstName.toLowerCase()}.${entity.first_lastName.toLowerCase()}`
    );
    const filter = await _modelRepository.filterByParam('email', valueRegex);
    if (filter.length) {
      const lastValue = filter[filter.length - 1];
      email = getEmail(lastValue.email, entity);
      entity = { ...entity, email };
    }
    const employeeCreated = await _modelRepository.create(entity);
    return mapperToClientSide([employeeCreated]);
  }

  async update(id, entity) {
    let email = entity.email;
    const valueRegex = new RegExp(email);
    const filter = await _modelRepository.filterByParam('email', valueRegex);
    if (filter.length > 1) {
      const lastValue = filter[filter.length - 1];
      email = getEmail(lastValue.email, entity);
      entity = { ...entity, email };
    }
    const employeeUpdated = await _modelRepository.update(id, entity);
    return mapperToClientSide([employeeUpdated]);
  }

  async delete(id) {
    return await _modelRepository.delete(id);
  }
}

module.exports = BaseServices;
