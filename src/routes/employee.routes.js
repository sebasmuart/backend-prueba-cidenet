module.exports = (router, EmplyeeController) => {
  router
    .get('/all', EmplyeeController.list)
    .get('/search/:value', EmplyeeController.search)
    .get('/:employeeId', EmplyeeController.get)
    .post('/', EmplyeeController.create)
    .patch('/:employeeId', EmplyeeController.update)
    .delete('/:employeeId', EmplyeeController.delete)
  return router;
};
