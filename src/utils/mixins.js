/**
 *
 * @param {*} email //las email registered in the Api
 * @param {*} entity //values of the employee to register
 */
function getEmail(email, entity) {
  let newEmail;
  const [valueSplitedByAt] = email.split('@');
  const valueSplitedByDot = valueSplitedByAt.split('.');
  const indexValue = valueSplitedByDot[valueSplitedByDot.length - 1];
  const emailDomain = getEmailDomain(entity);
  const first_lastName = entity.first_lastName.toLowerCase().replace(' ', '');
  if (Number.isInteger(parseInt(indexValue))) {
    const number = parseInt(indexValue);
    newEmail = `${entity.firstName.toLowerCase()}.${first_lastName}.${
      number + 1
    }@${emailDomain}`;
  } else {
    newEmail = `${entity.firstName.toLowerCase()}.${first_lastName}.${1}@${emailDomain}`;
  }
  return newEmail;
}

/**
 *
 * @param {e.Model} entity //values of the employee to register
 */
function getEmailDomain(entity) {
  let domain = 'cidenet.com.co';
  if (entity.country == 'USA') domain = 'cidenet.com.us';
  return domain;
}

module.exports = { getEmail };
