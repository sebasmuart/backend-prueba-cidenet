const { Schema, model } = require('mongoose');

const employeeSchema = new Schema(
  {
    firstName: String,
    secondName: String,
    first_lastName: String,
    second_lastName: String,
    state: { type: String, default: "Activo" },
    country: String,
    email: String,
    typeId: String,
    uId: String,
    admissionDate: String,
    jobArea: String,
  },
  { timestamps: true }
);

module.exports = model('Employee', employeeSchema);

