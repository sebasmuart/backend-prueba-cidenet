const BaseRepositories = require('../base.repository');
let _db = null;
class EmployeeRepositories extends BaseRepositories {
  constructor(db) {
    super(db, 'Employee');
    _db = db;
  }

  async filterByParam(filter, valueRegex) {
    const user = await _db['Employee'].find({ [filter]: valueRegex });
    return user;
  }

  async getCount() {
    return await _db['Employee'].countDocuments()
  }

  async searchByValues(value) {
    const regexValue = new RegExp(value);
    const employees = await _db['Employee'].find({
      $or: [
        { firstName: regexValue },
        { secondName: regexValue },
        { first_lastName: regexValue },
        { second_lastName: regexValue },
        { country: regexValue },
        { typeId: regexValue },
        { uId: regexValue },
        { email: regexValue },
        { jobArea: regexValue }
      ],
    });
    return employees;
  }
}

module.exports = EmployeeRepositories;
