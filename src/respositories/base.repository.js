let _db = null;
let _entity = null;

class BaseRepositories {
  constructor(db, entity) {
    _db = db;
    _entity = entity;
  }

  async getAll(pageSize = 10, pageNum = 1) {
    const skips = pageSize * (pageNum - 1);
    const employees = await _db[_entity].find().skip(skips).limit(pageSize);
    return employees;
  }

  async get(id) {
    const employees = await _db[_entity].findById(id);
    console.log(employees);
    return employees;
  }

  async create(entity) {
    const createdEmployee = _db[_entity].create(entity);
    return createdEmployee;
  }

  async update(id, entity) {
    const createdEmployee = await _db[_entity].findByIdAndUpdate(id, entity, {
      new: true,
    });
    return createdEmployee;
  }

  async delete(id) {
    await _db[_entity].findByIdAndDelete(id);
    return true;
  }
}

module.exports = BaseRepositories;
