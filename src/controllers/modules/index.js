const employee = require('./employee');

module.exports = class Controllers {
    constructor(router, services) {
        this.router = router;
        this.services = services
        //console.log(this.services);
        this.employee = employee(this.router, this.services);
    }
}