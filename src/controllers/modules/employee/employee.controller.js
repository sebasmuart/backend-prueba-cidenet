let _employeServices = null;

module.exports = class EmployeeController {
  constructor(EmployeeServices) {
    _employeServices = EmployeeServices;
  }

  async list(req, res, next) {
    const { pageSize, pageNum } = req.query;
    try {
      const el = await _employeServices.getAll(
        parseInt(pageSize),
        parseInt(pageNum)
      );
      return res.status(200).json(el);
    } catch (error) {
      next(error);
    }
  }

  async get(req, res, next) {
    const {
      params: { employeeId },
    } = req;
    try {
      const [employee] = await _employeServices.get(employeeId);
      return res.status(200).json(employee);
    } catch (error) {
      next(error);
    }
  }

  async create(req, res, next) {
    const { body } = req;
    try {
      const [createdEmployee] = await _employeServices.create(body);
      return res
        .status(201)
        .json({ msg: 'Employee created succesfully', data: createdEmployee });
    } catch (error) {
      next(error);
    }
  }

  async update(req, res, next) {
    const {
      body,
      params: { employeeId },
    } = req;
    try {
      const [updatedEmployee] = await _employeServices.update(employeeId, body);
      return res
        .status(200)
        .json({ msg: 'Employee updated succesfully', data: updatedEmployee });
    } catch (error) {
      next(error);
    }
  }

  async delete(req, res, next) {
    const {
      params: { employeeId },
    } = req;
    try {
      await _employeServices.delete(employeeId);
      return res.status(200).json({ msg: 'Employee deleted succesfully' });
    } catch (error) {
      next(error);
    }
  }

  async search(req, res, next) {
    const {
      params: { value },
    } = req;
    try {
      const employees = await _employeServices.searchByValues(value);
      return res.status(200).json({ data: employees });
    } catch (error) {
      next(error);
    }
  }
};
