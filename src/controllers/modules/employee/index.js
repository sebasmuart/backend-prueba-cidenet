const EmployeeController = require('./employee.controller');
const EmployeeRoutes = require('../../../routes/employee.routes');

module.exports = (router, { employeeSerivices }) => {
  return EmployeeRoutes(router, new EmployeeController(employeeSerivices));
};
